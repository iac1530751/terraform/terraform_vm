#!/bin/bash

# Script that captures information about system like CPU,Memory,current date,IP,hostname
# and then dumps this information to /etc/system-info file. 

# File to store system information
INFO_FILE="/etc/system-info"

# Get the hostname
HOSTNAME=$(hostname)

# Get the current date
DATE=$(date)

# Get the IP address
IP=$(hostname -I | awk '{print $1}')

# Get total CPU cores
TOTAL_CPU=$(nproc)

# Get total memory in kilobytes
TOTAL_MEMORY=$(grep MemTotal /proc/meminfo | awk '{print $2}')

# Convert total memory to gigabytes
TOTAL_MEMORY_GB=$(echo "scale=2; $TOTAL_MEMORY / 1024 / 1024" | bc)

# Create a header for the information
echo "System Information" > "$INFO_FILE"
echo "------------------" >> "$INFO_FILE"

# Append the information to the file
echo "Hostname: $HOSTNAME" >> "$INFO_FILE"
echo "Date of Installation: $DATE" >> "$INFO_FILE"
echo "IP Address: $IP" >> "$INFO_FILE"
echo "Total CPU Cores: $TOTAL_CPU" >> "$INFO_FILE"
echo "Total Memory: ${TOTAL_MEMORY_GB} GB" >> "$INFO_FILE"

# Display a message
echo "System information has been collected and stored in $INFO_FILE."
