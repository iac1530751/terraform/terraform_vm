
variable "resource_group_name" {
  type        = string
  description = "RG name in Azure"
  default     = "sb-app"
}

variable "resource_location" {
  type        = string
  description = "RG location in Azure"
  default     = "West Europe"
}

variable "env_tag" {
  type        = string
  description = "Tags for the enviroment"
  default     = "test"
}

variable "username" {
  type        = string
  description = "The username for the local account that will be created on the new VM."
  default     = "azureadmin"
}