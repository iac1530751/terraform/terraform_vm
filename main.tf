
resource "azurerm_resource_group" "TF_rg" {
  name     = "${var.resource_group_name}-${var.env_tag}-rg"
  location = var.resource_location

  tags = {
    environment = var.env_tag
  }

}

resource "azurerm_virtual_network" "TF_vnet" {
  name                = "${var.resource_group_name}-${var.env_tag}-vnet"
  resource_group_name = azurerm_resource_group.TF_rg.name
  location            = azurerm_resource_group.TF_rg.location
  address_space       = ["10.123.0.0/16"]

  tags = {
    environment = var.env_tag
  }

}

resource "azurerm_subnet" "TF_vnet_subnet1" {
  name                 = "${var.resource_group_name}-${var.env_tag}-subnet1"
  resource_group_name  = azurerm_resource_group.TF_rg.name
  virtual_network_name = azurerm_virtual_network.TF_vnet.name
  address_prefixes     = ["10.123.1.0/24"]
}

resource "azurerm_network_security_group" "TF_vnet_sg" {
  name                = "${var.resource_group_name}-${var.env_tag}-vnet-sg"
  location            = azurerm_resource_group.TF_rg.location
  resource_group_name = azurerm_resource_group.TF_rg.name

  tags = {
    environment = var.env_tag
  }

}

resource "azurerm_network_security_rule" "TF_vnet_sg_rule" {
  name                        = "${var.resource_group_name}-${var.env_tag}-vnet-sg-rule"
  priority                    = 100
  direction                   = "Inbound"
  access                      = "Allow"
  protocol                    = "Tcp"
  source_port_range           = "*"
  destination_port_range      = "*"
  source_address_prefix       = "*"
  destination_address_prefix  = "*"
  resource_group_name         = azurerm_resource_group.TF_rg.name
  network_security_group_name = azurerm_network_security_group.TF_vnet_sg.name
}

resource "azurerm_subnet_network_security_group_association" "TF_vnet_sg_rule_asso" {
  subnet_id                 = azurerm_subnet.TF_vnet_subnet1.id
  network_security_group_id = azurerm_network_security_group.TF_vnet_sg.id
}


########### VM starts from here 

resource "azurerm_public_ip" "TF_VM_IP" {
  name                = "${var.resource_group_name}-${var.env_tag}-VM-IP"
  resource_group_name = azurerm_resource_group.TF_rg.name
  location            = azurerm_resource_group.TF_rg.location
  allocation_method   = "Dynamic"

  tags = {
    environment = var.env_tag
  }

}

resource "azurerm_network_interface" "TF_VM_nic" {
  name                = "${var.resource_group_name}-${var.env_tag}-VM-nic"
  location            = azurerm_resource_group.TF_rg.location
  resource_group_name = azurerm_resource_group.TF_rg.name

  ip_configuration {
    name                          = "internal"
    subnet_id                     = azurerm_subnet.TF_vnet_subnet1.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = azurerm_public_ip.TF_VM_IP.id
  }

  tags = {
    environment = var.env_tag
  }

}

# Generate random text for a unique storage account name
resource "random_id" "random_id" {
  keepers = {
    # Generate a new ID only when a new resource group is defined
    resource_group = azurerm_resource_group.TF_rg.name
  }

  byte_length = 8
}

# Create storage account for boot diagnostics
resource "azurerm_storage_account" "TF_storage_account" {
  name                     = "diag${random_id.random_id.hex}"
  location                 = azurerm_resource_group.TF_rg.location
  resource_group_name      = azurerm_resource_group.TF_rg.name
  account_tier             = "Standard"
  account_replication_type = "LRS"
}

# Create virtual machine
resource "azurerm_linux_virtual_machine" "TF_VM" {
  name                  = "${var.resource_group_name}-${var.env_tag}-VM"
  location              = azurerm_resource_group.TF_rg.location
  resource_group_name   = azurerm_resource_group.TF_rg.name
  network_interface_ids = [azurerm_network_interface.TF_VM_nic.id]
  size                  = "Standard_DS1_v2"

  os_disk {
    name                 = "myOsDisk"
    caching              = "ReadWrite"
    storage_account_type = "Premium_LRS"
  }

  source_image_reference {
    publisher = "Canonical"
    offer     = "0001-com-ubuntu-server-jammy"
    sku       = "22_04-lts-gen2"
    version   = "latest"
  }

  computer_name  = "hostname"
  admin_username = var.username

  admin_ssh_key {
    username   = var.username
    public_key = jsondecode(azapi_resource_action.ssh_public_key_gen.output).publicKey
  }

  boot_diagnostics {
    storage_account_uri = azurerm_storage_account.TF_storage_account.primary_blob_endpoint
  }

  # Execute the script as soon system is installed. 
  custom_data = filebase64("Scripts/system_info.sh")

}