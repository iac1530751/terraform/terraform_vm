# Terraform VM

Terraform code in this repo will deploy Azure resources - 
* Network Components - RG,Vnet,Subnet,Security Group,Security Group rule,
* Compute Components - VM,Public IP, NIC, Storage account, ssh_keys
* Running Script - Run script from /Script directory once the VM is deployed. 

### Deploying VM and infra -
Below are the steps that needs to be carried out to deploy the Infra on Azure

> **Note -** Make sure that rigt variables are declared in terraform.tfvars before running any command. 

```
# cd production/
# terraform init 

# terraform plan 
# terraform apply -auto-approve

# terraform state list 
```

> **Note -** Here, we are using the ssh.tf to create the keys. It is also possible that your exsistings keys can be added to the server.

Further Plans -
* Copy your keys instead of creating one. 
* Create modules making it possible to deploy more number of VMs depinding on the VM cound provided. 
